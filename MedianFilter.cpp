/*
 * cv::medianBlur(InputArray src, OutputArray dst, int ksize)
 * 中值滤波
 * 将图像的每个像素用邻域(以当前像素为中心的正方形区域)像素的中值代替
 * 在去除椒盐噪声中最有效
 *
 *
 * cv:: blur(InputArray src, OutputArray dst, Size ksize,
 *          Point anchor=Point(-1,-1), int borderType=BORDER_DEFAULT)
 * 均值滤波
 * 将图像的每个像素用邻域像素的平均值代替
 * 会导致图像模糊
 *
 */

#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace cv;

int main(int argc, char *argv[])
{
    if (argc != 2) {
        std::cerr << "Usage: MedianFilter image.jpg" << std::endl;
        return -1;
    }

    const int ksize = 3;
    const Point center(-1, -1);     // kernel center
    Mat srcImage = imread(argv[1], 1);
    Mat meanImage = srcImage.clone();
    Mat medianImage = srcImage.clone();

    blur(srcImage, meanImage, Size(ksize, ksize), center, BORDER_DEFAULT);
    medianBlur(srcImage, medianImage, ksize);

    imshow("src", srcImage);
    imshow("mean", meanImage);
    imshow("median", medianImage);
    waitKey();

    return 0;
}
